const http  = require('http');

const app = require('./config/express')();
const connection = require('./config/database.js')();
const env = app.get('env')();

http.createServer(app)
	.listen(
		env.port,
		function(){
			console.log(`BACKEND is running on port ${env.port}.`);		
		});