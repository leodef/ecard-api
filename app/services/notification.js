//frameworks
const nodemailer = require('nodemailer');
const moment = require("moment");
const _ = require('lodash');

//app
const env = require('../config/env')();

//services
const GatewayService = require('./gateway');

transporterConfig = {
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false,
    auth: {
        user: 'agga4p5bepamg6i2@ethereal.email',
        pass: 'K8mDHRZEzkTdCef73w',
    }
}

defaultMailOptions = {
    from: '"Ecard 👻" <no-reply@ecard.com>',
    //to: 'bar@example.com, baz@example.com',
    ///subject: 'Hello ✔', 
    //text: 'Hello world?', 
    //html: '<b>Hello world?</b>'
};

class NotificationService {
    
    constructor(){
        this.transporter = nodemailer.createTransport(transporterConfig);
    }

    getMailOptions(param) {
        return _.assign(param, defaultMailOptions);
    }

    sendMail(options, callback) {
        return this.transporter.sendMail(this.getMailOptions(options), callback);
        /*
        (err, info) => {
          if (err) { return log(err); }
          log('Message sent: %s', info.messageId);
          log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        });*/
    }

    //enviar token
    confirmAccount(token, url, callback) {

        const email = token.account.email;
        const subject = 'ECard - Confirm Account ✔';
        if(!GatewayService.checkUrl(url)){
           url = env.urlConfig.confirmAccount
        }
        url = url.replace(':token', token._id);
        const text = 'Confirm Account Link - ' + url;
        const html = `
      <h3>Confirm Account</h3>
      <br/>
      <p>
        <a href="${url}">
          Click here for active account
        </a>
      </p>`;

        this.sendMail(
            {
                to: email,
                subject: subject,
                text: text,
                html: html
            }, callback);

    }

    //informar sucesso na operação
    confirmAccountSuccess(account, callback) {
        const email = account.email;
        const subject = 'ECard - Account Successful Activated ✔';
        const text = 'Account Successful Activated ';
        const html = `<h3>Account Successful Activated</h3><br/>`;

        this.sendMail(
            {
                to: email,
                subject: subject,
                text: text,
                html: html
            }, callback);
    }

    //enviar token
    forgotPassword(token, url, callback) {
        const email = token.account.email;
        const subject = 'ECard - Reset Password Request ✔';
        if(!GatewayService.checkUrl(url)){
            url = env.urlConfig.forgotPassword
         }
        url = url.replace(':token', token._id);
        const text = 'Reset Password Request Link - ' + url;
        const html = `<h3>Reset Password Request</h3><br/>
        <p>
            <a href="${url}">
                Click here for reset password
            </a>
        </p>`;
        this.sendMail(
            {
                to: email,
                subject: subject,
                text: text,
                html: html
            }, callback);
    }

    //informar sucesso na operação
    resetPasswordSuccess(account, callback) {
        const email = account.email;
        const subject = 'ECard - Password Successful Reseted ✔';
        const text = 'Password Successful Reseted';
        const html = `<h3>Password Successful Reseted</h3><br/>`;

        this.sendMail(
            {
                to: email,
                subject: subject,
                text: text,
                html: html
            }, callback);
    }
}

const notificationService = new NotificationService();
module.exports = notificationService;