const env = require('../config/env')();

class GatewayService {

    checkUrl(url){
        if(!url){return false;}
        return url.startsWith(env.urlConfig.host);
    };
}

const gatewayService = new GatewayService();
module.exports = gatewayService;