//frameworks
//const restful = require('node-restful');
//const _ = require('lodash');
const mongoose = require('mongoose');
const moment = require("moment");

//app
const ACCOUNT_TOKEN_TYPE = require('../../enums/account-token-type');
const cardSchema = require('../../schemas/card');
const userCardSchema = require('../../schemas/user-card');

const CardModel = mongoose.model('Card', cardSchema);
const UserCardModel = mongoose.model('UserCard', userCardSchema);


class PersonSectionService {

    constructor(){}

    all(req, res, next){ // / get
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;

        SectionModel.find({}, 
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }

    find(req, res, next){ // /:id get
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //id
        const id = req.params.id;

        SectionModel.findById(id,
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }

    delete(req, res, next){ // /:id delete
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //id
        const id = req.params.id;

        SectionModel.findOneAndRemove(
            {_id: id}, 
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }

    create(req, res, next){ // / post
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;
        //dto
        const createDTO = {
            title: body.title,
        }
        //create card user
        SectionModel.create(
            createDTO,
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
        });
    }

    update(req, res, next){ // /:id update
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;
        //id
        const id = req.params.id;
        //dto
        const updateDTO = {
            title: body.title
        }
        
        SectionModel.findOneAndUpdate(
            {_id: id},
            updateDTO,
            {new: true},
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
            return;
        });
    }
}

const personSectionService = new PersonSectionService();
module.exports = personSectionService;