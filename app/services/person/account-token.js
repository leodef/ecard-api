//frameworks
//const restful = require('node-restful');
//const _ = require('lodash');
const mongoose = require('mongoose');
const moment = require("moment");

//config
const RESET_PASSWORD_EXPIRY_DAYS = 30;
const CONFIRM_ACCOUNT_EXPIRY_DAYS = 30;
const CONFIRM_ACCOUNT = "CONFIRM_ACCOUNT";
const RESET_PASSWORD = "RESET_PASSWORD";
//app
const ACCOUNT_TOKEN_TYPE = require('../../enums/account-token-type');
const accountTokenSchema = require('../../schemas/account-token');
const AccountTokenModel = mongoose.model('AccountToken', accountTokenSchema);

//services
const NotificationService = require('../notification');

class PersonAccountTokenService {

  constructor(){
    
  }

  //createResetPasswordToken
  createResetPasswordToken(account, url, callback) {
    return AccountTokenModel.create({
      expiryDate: moment().add(RESET_PASSWORD_EXPIRY_DAYS, 'days').calendar(),
      type: RESET_PASSWORD,
      account: account
    }, function(err, token){
      if(err){
        callback(err, token); 
        return;
      }
      NotificationService.forgotPassword(account, url, function(nErr, nInfo){
        callback(err, token);
      });
    });
  }

  //createConfirmAccountToken
  createConfirmAccountToken(account, url, callback) {
    
    return AccountTokenModel.create({
      expiryDate: moment().add(CONFIRM_ACCOUNT_EXPIRY_DAYS, 'days').calendar(),
      type: CONFIRM_ACCOUNT,
      account: account
    }, function(err, token){
      if(err){  callback(err, token); return;}
      NotificationService.confirmAccount(token, url, function(nErr, nInfo){
        callback(err, token);
      });
    });
  }

  //confirmAccount
  confirmAccount(id, callback){
    console.log('id: '+id+' type: '+CONFIRM_ACCOUNT);
    return AccountTokenModel.findOneAndRemove({_id: id, type: CONFIRM_ACCOUNT})
      .populate('account')
      .exec(function(err, token){
        if(err){callback(err, token);  return; }
      NotificationService.confirmAccountSuccess(token.account, function(nErr, nInfo){
        callback(err, token);
      });
    });
  }

  //resetPassword
  resetPassword(id, callback){
    return AccountTokenModel.findOneAndRemove({_id: id, type: RESET_PASSWORD})
      .populate('account')
      .exec(function(err, token){
      if(err ){callback(err, token);  return; }

      NotificationService.resetPasswordSuccess(token.account, function(nErr, nInfo){
        callback(err, token);
      });
    });
  }
}

const personAccountTokenService = new PersonAccountTokenService();
module.exports = personAccountTokenService;