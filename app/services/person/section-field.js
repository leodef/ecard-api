
//frameworks
//const restful = require('node-restful');
//const _ = require('lodash');
const mongoose = require('mongoose');
const moment = require("moment");

//app
const sectionFieldSchema = require('../../schemas/section-field');
const sectionSchema = require('../../schemas/section');

//model
const SectionFieldModel = mongoose.model('SectionField', sectionFieldSchema);
const SectionModel = mongoose.model('Section', sectionSchema);


class PersonSectionFieldService {

    constructor(){}

    bySection(req, res, next){// /bySection/:id get
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;

        //user id
        const userId = user._id;

        //section id
        const id = req.params.id;

        SectionModel.findById(id)
            .populate("sectionFieldList")
            .exec(function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result.sectionFieldList);
                return;
            });
    }
   
}

const personSectionFieldService = new PersonSectionFieldService();
module.exports = personSectionFieldService;