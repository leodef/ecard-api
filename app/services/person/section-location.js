
//frameworks
//const restful = require('node-restful');
//const _ = require('lodash');
const mongoose = require('mongoose');
const moment = require("moment");

//app
const sectionLocationSchema = require('../../schemas/section-location');
const sectionSchema = require('../../schemas/section');
const cardSchema = require('../../schemas/card');

//model
const SectionLocationModel = mongoose.model('SectionLocation', sectionLocationSchema);
const SectionModel = mongoose.model('Section', sectionSchema);
const CardModel = mongoose.model('Card', cardSchema);


class PersonSectionLocationService {

    constructor(){}

    bySection(req, res, next){// /bySection/:id get
        
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;

        //user id
        const userId = user._id;

        //section id
        const id = req.params.id;

        SectionModel.findById(id)
            .populate("sectionLocationList")
            .exec(function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result.sectionLocationList);
                return;
            });
    }
    
    byCard(req, res, next){// /byCard/:id get
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;

        //user id
        const userId = user._id;

        //section id
        const id = req.params.id;

        CardModel.findById(id)
            .populate("sectionLocationList")
            .exec(function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result.sectionLocationList);
                return;
            });
    }
   
}

const personSectionLocationService = new PersonSectionLocationService();
module.exports = personSectionLocationService;