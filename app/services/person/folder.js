
//frameworks
//const restful = require('node-restful');
//const _ = require('lodash');
const mongoose = require('mongoose');
const moment = require("moment");

//app
const folderSchema = require('../../schemas/folder');
const folderUserSchema = require('../../schemas/folder-user');

//model
const FolderModel = mongoose.model('Folder', folderSchema);
const FolderUserModel = mongoose.model('FolderUser', folderUserSchema);


class PersonFolderService {

    constructor(){}

    byFolder(req, res, next){// /byFolder/:id get
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;

        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;

        //user id
        const userId = user._id;

        //folder id
        const folderid = req.params.id;

        FolderModel
            .find({parent: folderid})
            .exec(function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }

    root(req, res, next){ // /root post
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;

        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;

        //user id
        const userId = user._id;

        const folderDTO = {
            title: body.title,
            isRoot: true,
            parent: null
        }

        FolderModel.create(
            folderDTO,
            function(err, folder){
                if (req.handleError(req, res, err)) { return; }
                if (req.confirmResult(req, res, folder)) { return; }
                const folderUserDTO = {
                    user: user,
                    folder: folder
                };
                
                FolderUserModel.create(
                    folderUserDTO,
                    function(err, folderUser){
                        if (req.handleError(req, res, err)) { return; }
                        if (req.confirmResult(req, res, folderUser)) { return; }
                        req.handleResponse(req, res, folderUser);
                        return;
                });// create folder
            });// create folder user
    }
   
    child(req, res, next){ // child/:id post
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;
        //id
        const id = req.params.id;

        FolderModel.findById(id,
            function(err, parent){
                if (req.handleError(req, res, err)) { return; }
                if (req.confirmResult(req, res, parent)) { return; }
                const childDTO = {
                    title: body.title,
                    isRoot: false,
                    parent: parent._id
                };
                FolderModel.create(
                    childDTO,
                    function(err, child){
                        if (req.handleError(req, res, err)) { return; }
                        if (req.confirmResult(req, res, child)) { return; }
                        req.handleResponse(req, res, child);
                        return;
                });
            });
    }

    all(req, res, next){  // / get
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;

        FolderModel.find({}, 
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }

    find(req, res, next){ // /:id get
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //id
        const id = req.params.id;

        FolderModel.findById(id,
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }

    delete(req, res, next){ // /:id delete
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //id
        const id = req.params.id;

        FolderModel.findOneAndRemove(
            {_id: id}, 
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }

    update(req, res, next){ // /:id update
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;
        //id
        const id = req.params.id;
        //dto
        const updateDTO = {
            title: body.title,
        }
        
        FolderModel.findOneAndUpdate(
            {_id: id},
            updateDTO,
            {new: true},
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
            return;
        });
    }
}


const personFolderService = new PersonFolderService();
module.exports = personFolderService;