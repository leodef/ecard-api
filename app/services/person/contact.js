//frameworks
//const restful = require('node-restful');
//const _ = require('lodash');
const mongoose = require('mongoose');
const moment = require("moment");

//app
const CONTACT_TYPE = require('../../enums/contact-type');
const contactSchema = require('../../schemas/contact');

const ContactModel = mongoose.model('Contact', contactSchema);


class PersonContactService {

    constructor(){}

    byFolder(req, res, next){// /byFolder/:id get
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;

        //user id
        const userId = user._id;

        //folder id
        const folderid = req.params.id;

        ContactModel
            .find({folder: folderid})
            .populate("contactId")
            .exec(function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }

    all(req, res, next){ // / get
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;

        ContactModel.find({}, 
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }

    find(req, res, next){ // /:id get
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //id
        const id = req.params.id;

        ContactModel.findById(id,
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }

    delete(req, res, next){ // /:id delete
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //id
        const id = req.params.id;

        ContactModel.findOneAndRemove(
            {_id: id}, 
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }

    create(req, res, next){ // / post
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;
        //dto
        const createDTO = {
            title: body.title,
            contactId: body.contactId,  // id referente ao objeto do contato (cartão, pessoa ou empresa)
            contactType: body.contactType, //se é cartão pessoas ou empresa
            user: user,
            folder: body.folder,
        }
        //create card user
        ContactModel.create(
            createDTO,
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
        });
    }

    update(req, res, next){ // /:id put
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;
        //id
        const id = req.params.id;
        //dto
        const updateDTO = {
            title: body.title
        }
        
        ContactModel.findOneAndUpdate(
            {_id: id},
            updateDTO,
            {new: true},
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
            return;
        });
    }

    updateFolder(req, res, next){ // folder/:id put
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;
        //id
        const id = req.params.id;
        //dto
        const updateDTO = {
            folder: body.folder
        }
        
        ContactModel.findOneAndUpdate(
            {_id: id},
            updateDTO,
            {new: true},
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
            return;
        });
    }
}

const personContactService = new PersonContactService();
module.exports = personContactService;