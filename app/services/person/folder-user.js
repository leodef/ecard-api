
//frameworks
//const restful = require('node-restful');
//const _ = require('lodash');
const mongoose = require('mongoose');
const moment = require("moment");

//app
const folderSchema = require('../../schemas/folder');
const folderUserSchema = require('../../schemas/folder-user');

//model
const FolderModel = mongoose.model('Folder', folderSchema);
const FolderUserModel = mongoose.model('FolderUser', folderUserSchema);


class PersonFolderUserService {

    constructor(){}

    byUser(req, res, next){// /byUser get

        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;

        FolderUserModel.find({user: user})
            .populate("folder")
            .exec(function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }
    
}

const personFolderUserService = new PersonFolderUserService();
module.exports = personFolderUserService;