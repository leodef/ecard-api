//frameworks
//const restful = require('node-restful');
const mongoose = require('mongoose');
const moment = require("moment");
const bcrypt = require('bcrypt');
const _ = require('lodash');

//config
const USER = "USER";
const AUTHENTICATION_TOKEN_EXPIRY_DAYS = 6;

//app
const accountSchema = require('../../schemas/account');
const userSchema = require('../../schemas/user');
const authenticateTokenSchema = require('../../schemas/authenticate-token');

const PersonAccountModel = mongoose.model('Account', accountSchema);
const PersonUserModel = mongoose.model('User', userSchema);
const AuthenticateTokenModel = mongoose.model('AuthenticateToken', authenticateTokenSchema);
//services
const PersonAccountTokenService = require('./account-token');

class PersonAccountService {
    
    //login
    login (req, res, next) { //login.post
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;

        const loginDTO = {
            username: body.username,
            password: body.password,
        };
        // find account
        PersonAccountModel.findOne(
            { 
                $or:[ 
                    {username: loginDTO.username},
                    {email: loginDTO.username}
                ]
            },
            function(err, account){
                if (req.handleError(req, res, err)) { return; }
                if (req.confirmResult(req, res, account)) { return; }
                const authenticateTokenDTO = {
                    account: account,
                    expiryDate: moment().add(AUTHENTICATION_TOKEN_EXPIRY_DAYS, 'days').toDate()
                };
                if(bcrypt.compareSync(loginDTO.password, account.password)) {
                    // create or update token
                    console.log(authenticateTokenDTO);
                    AuthenticateTokenModel.findOneAndUpdate(
                        {account: account._id},
                        authenticateTokenDTO,
                        {upsert: true, new: true, runValidators: true}, 
                        function(err, authenticateToken){
                            if (req.handleError(req, res, err)) { return; }
                            req.handleSuccess(req, res, 'OK', 'OK', {token: authenticateToken._id});
                            return;
                        });// create or update token
                }else{
                    req.handleError({title: 'Invalid Password'}, req, res);
                    return;
                }
            });//find account
    }

    //logout
    logout (req, res, next) { //logout.post
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        const account = user.account;
        // create or update token
        AuthenticateTokenModel.findOneAndRemove(
            {account: account},
            function(err, authenticateToken){
                if (req.handleError(req, res, err)) { return; }
                req.handleSuccess(req, res, 'OK', 'OK');
                return;
            });// create or update token
    }

    //register
    register(req, res, next) {//register.post
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;
        const url = body.url;
        if (!body) { req.handleError({ title: 'Erro' }, req, res) }
        const accountDTO = {
            name: body.name,
            username: body.username,
            email: body.email,
            password: bcrypt.hashSync(body.password, 10),
            accountType: USER,
            gender: body.gender,
            birth: moment(body.birth), // YYYY-MM-DD HH:mm:SS
            enabled: false
        }
        //create account
        PersonAccountModel.create(accountDTO, function (err, savedAccount) {
            if (req.handleError(req, res, err)) { return; }
            
            const userDTO = {
                nickname: savedAccount.name,
                account: savedAccount,
            }

            // create user
            PersonUserModel.create(userDTO, function(err, savedUser){
                if (req.handleError(req, res, err)) { return; }
                if (req.confirmResult(req, res, savedUser)) { return; }
                // create token
                PersonAccountTokenService.createConfirmAccountToken(savedAccount, url, function (tErr, savedAccountToken) {
                    if (req.handleError(tErr, req, res)) { return; }
                    if (req.confirmResult(req, res, savedAccountToken)) { return; }
                    req.handleSuccess(req, res, 'OK', 'OK');
                    return;
                });// create token
            });// create user
        });//create token
    }

    //confirm-password
    confirmAccount (req, res, next) { //confirm-account/:token.post
        const token = req.params.token;
        PersonAccountTokenService.confirmAccount(token, function(err, tokenResult){
            if (req.handleError(req, res, err)) { return; }
            if (req.confirmResult(req, res, tokenResult)) { return; }
            const account = tokenResult.account;
            PersonAccountModel.findByIdAndUpdate(account._id, {enabled: true}, function(aErr, accountResult){
                if (req.handleError(req, res, aErr)) { return; }
                if (req.confirmResult(req, res, accountResult)) { return; }
                req.handleSuccess(req, res, 'OK', 'OK');
                return;
            });
        });
    }

    //forgot-password
    forgotPassword(req, res, next) { //forgot-password.post
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;
        const url = body.url;
        const email = body.email;
        PersonAccountModel.findOne({email:email}, function(err, account){
            if (req.handleError(req, res, err)) { return; }
            if (req.confirmResult(req, res, account)) { return; }
            PersonAccountTokenService.createResetPasswordToken(account, url, function(tErr, token){
                if (req.handleError(tErr, req, res)) { return; }
                if (req.confirmResult(req, res, token)) { return; }
                req.handleSuccess(req, res, 'OK', 'OK');
                return;
            });
        });
        
        res.send('forgot-password.post');
    }

    //reset-password
    resetPassword(req, res, next) {//reset-password:token.post
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;
        const resetPasswordDTO = {
            token: body.token,
            password: body.password,
            confirmPassword: body.confirmPassword
        }
        if(resetPasswordDTO.password !== resetPasswordDTO.confirmPassword){
            req.handleError("password and confirmPasword must match", req, res);
            return;
        }
        PersonAccountTokenService.resetPassword(token, function(err, token){
            if (req.handleError(req, res, err)) { return; }
            if (req.confirmResult(req, res, token)) { return; }
            const account = token.account;
            account.password = resetPasswordDTO.password;
            account.save(function(aErr, account){
                if (req.handleError(aErr, req, res)) { return; }
                if (req.confirmResult(req, res, account)) { return; }
                req.handleSuccess(req, res, 'OK', 'OK');
                return;
            })
        });
        res.send('reset-password:token.post token=' + token);
    }

    //check-username
    checkUsername(req, res, next) { //check-username/:username.get
        var username = req.params.username || '';
        return PersonAccountModel.findOne(
            {username: username},
            function(err, token){
                if(err){callback(err, token);  return; }
                if(token){
                    res.json({isValid: false});
                    return;
                }
                res.json({isValid: true});
                return;
        });
    }
    /*
        //List Options like
        var query = PersonAccountModel.find({});
        query.where({ 'username': new RegExp(username, 'i') }) //'/'+data+'/i'
        query.select('username');
        query.limit(500);
        //query.skip(100);
        query.exec(function (err, response) {
            if (req.handleError(req, res, err)) { return; }
            res.json(response);
        }); */
}

//AccountService.methods(['get', 'post', 'put', 'delete']);
//AccountService.updateOptions({ new: true, runValidators: true });
const personAccountService = new PersonAccountService();
module.exports = personAccountService;