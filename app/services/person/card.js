//frameworks
//const restful = require('node-restful');
//const _ = require('lodash');
const mongoose = require('mongoose');
const moment = require("moment");

//app
const ACCOUNT_TOKEN_TYPE = require('../../enums/account-token-type');
const cardSchema = require('../../schemas/card');
const userCardSchema = require('../../schemas/user-card');

const CardModel = mongoose.model('Card', cardSchema);
const UserCardModel = mongoose.model('UserCard', userCardSchema);


class PersonCardService {

    constructor(){}

    rowByUser(req, res, next){// /rowByUser get
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;

        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;
        
        //user id
        const userId = user._id;

        UserCardModel
            .aggregate([
                {$match: 
                    { 
                        user:  userId // find where user.id = userId
                    }
                },
                {$lookup:
                    {
                    from: 'cards',
                    localField: 'card',
                    foreignField:'_id',
                    as: 'cards'
                    }//https://docs.mongodb.com/manual/reference/operator/aggregation/lookup/#lookup-single-equality
                },
                {$replaceRoot: { 
                        newRoot: { 
                            $arrayElemAt: [ "$cards", 0 ]
                           /* $mergeObjects: [ Mistura os 2 objetos adicionando os novos campos
                                { $arrayElemAt: [ "$cards", 0 ] }
                                , "$$ROOT"
                            ] */
                        } 
                    }
                }
                //{ $replaceRoot: { newRoot: 'card' } } // return list of cards intead of userCard
            ])
            .exec(function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }

    addSectionLocation(req, res,next){ // /:id/section post
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;

        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;
        
        CardModel.findById(id,
            function(err, card){
                if (req.handleError(req, res, err)) { return; }
                
                const sectionDTO = {
                    title: body.title,
                    color: body.color,
                    columnN: 0,
                    rowN: 0,
                };

                SectionModel.create(
                    sectionDTO,
                    function(err, section){
                        if (req.handleError(req, res, err)) { return; }
                        
                        const sectionLocationDTO = {
                            title: body.title,
                            contactId: body.contactId,  // id referente ao objeto do contato (cartão, pessoa ou empresa)
                            contactType: body.contactType, //se é cartão pessoas ou empresa
                            user: user,
                            folder: body.folder,
                        };

                        SectionLocationModel.create(
                            sectionLocationDTO,
                            function(err, sectionLocation){
                                if (req.handleError(req, res, err)) { return; }
                                
                                card.sectionLocationList.push(sectionLocation);
                                card.save(function(err, savedCard){
                                    req.handleResponse(req, res, savedCard);
                                    return;
                                }); // save card
                        }); // sectionLocation
                }); // section
            }); // card
    }

    all(req, res, next){ // / get
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;

        CardModel.find({}, 
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }

    find(req, res, next){ // /:id get
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //id
        const id = req.params.id;

        CardModel.findById(id,
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }

    delete(req, res, next){ // /:id delete
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //id
        const id = req.params.id;

        CardModel.findOneAndRemove(
            {_id: id}, 
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
            });
    }

    create(req, res, next){ // / post
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;
        //dto
        const createDTO = {
            title: body.title,
            language: body.language,
            color: body.color,
            traking: body.traking,
            type: body.type, //  (BUSINESS_CARD, CUSTOM_LABEL, CATALOG, MENU)
            code: body.code
        };
        //create card user
        CardModel.create(
            createDTO,
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
                return;
        });
    }

    update(req, res, next){ // /:id update
        //user
        if(req.confirmAuth(req, res)){return;}
        const user = req.user;
        //body
        if(req.confirmBody(req, res)){return;}
        const body = req.body;
        //id
        const id = req.params.id;
        //dto
        const updateDTO = {
            title: body.title,
            language: body.language,
            color: body.color,
            traking: body.traking,
            type: body.type //  (BUSINESS_CARD, CUSTOM_LABEL, CATALOG, MENU)
        };
        
        CardModel.findOneAndUpdate(
            {_id: id},
            updateDTO,
            {new: true},
            function(err, result){
                if (req.handleError(req, res, err)) { return; }
                req.handleResponse(req, res, result);
            return;
        });
    }
}

const personCardService = new PersonCardService();
module.exports = personCardService;