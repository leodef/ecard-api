const express = require('express');
const mongoose = require('mongoose');
const moment = require('moment');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const passport = require('passport');
const helmet = require('helmet');
const env = require('./env');
const routes = require('../routes');
const morgan = require('morgan');
const cors = require('cors');
const Strategy = require('passport-http-bearer').Strategy;


const userSchema = require('../schemas/user');
const authenticateTokenSchema = require('../schemas/authenticate-token');

const UserModel = mongoose.model('User', userSchema);
const AuthenticateTokenModel = mongoose.model('AuthenticateToken', authenticateTokenSchema);

function authenticate(token, cb) {
	// find account
	AuthenticateTokenModel.findById(
		token, 
		function(err, authenticateToken) {
			
			if (err) { return cb(err); }
			if (!authenticateToken) { return cb(null, false); }

			// check expiry date
			if(moment() >= authenticate.expiryDate){
				// remove token
				authenticateToken.remove(function(err, token){
					return cb(err, false);
				});
			}else{
				// find user
				UserModel
					.findOne({account: authenticateToken.account})
					.populate('account')
					.exec(function(err, user){
						if (err) { return cb(err); }
						if (!user) { return cb(null, false); }
						return cb(null, user);// find user
					});
			}
	});// find account
};

const corsOptions = {
  origin: '*',//'http://example.com',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

module.exports = function(){
	const app = express();



	app.use(cors(corsOptions));  
	app.use(morgan('tiny'));
	//app.use(express.static('./public'));
	app.set('env', env);
	app.use(bodyParser.urlencoded({ extended: false }));
	app.use(bodyParser.json());
	app.use(methodOverride());
	
	
	//realiza o parser do header de cookies da requisição populando req.cookies e armazena o IDdasessão
	app.use(cookieParser());//req.cookie

	//cria por padrão a sessão do usuário em memória
	app.use(session({//req.session
		secret: 'a4f8071f-c873-4447-8ee2',
		cookie: { maxAge: 2628000000 },
		resave: true,
		//garante que as informações da sessão serão acessíveis através decookiesacadarequisição
		saveUninitialized: true
	}));
	
	app.use(helmet.ieNoOpen());
	app.use(helmet.frameguard());// impede que a pagina seja acessada por xframes
	app.use(helmet.xssFilter());
	app.use(helmet.noSniff());// impede que o browser use scrpts ou links com arquivos que não sejam css ou js
	//app.disable('x-powerd-by');
	app.use(helmet.hidePoweredBy({setTo:'PHP 5.5.14'}));

	// app.use(passport.initialize());
	// app.use(passport.session());
	// passport.authenticate('bearer', { session: false })
	passport.use(new Strategy(authenticate));

	app.use(function(req, res, next){
		
		req.handleError = function(req, res, err, status = 500){
			if(!err){return false;}
			console.log(err);
			if(!err.error){
				err.error = 'true';
			}
			res.status(status).json(err);
			return true;
		};

		req.handleSuccess= function(req, res, title, description, data = null, status = 200){
			res.status(status).json({
				title: title,
				description: description,
				data:data
			});
		};

		req.handleResponse= function(req, res, data, status = 200){
			res.status(status).json(data);
		};
		
		req.confirmAuth = function(req, res){
			if(!req.user){
				req.handleError(req, res, {title: 'Authentication Falied'}, 401);
				return true;
			}
			return false;			
		};

		req.confirmPermission = function(req, res, user){
			if(req.confirmAuth(req, res)){
				return true;
			}
			var userid;
			userid = user._id ? user._id : user;
			userid = user.id ? user.id : user;
			if(userid != req.user._id){
				req.handleError(req, res, {title: 'Wrong Permission'}, 403);
				return true;
			}
			return false;

		};

		req.confirmBody = function(req, res){
			if(!req.body){
				req.handleError(req, res, {title: 'Empty body'});
				return true;
			}
			return false;			
		};

		req.confirmResult = function(req, res, result){
			if(!result){
				req.handleError(req, res, {title: 'Object Not Found'}, 404);
				return true;
			}
			return false;
		}

		next();
	});

	app.use(routes);

	return app;
}