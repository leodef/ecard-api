module.exports = {
	env: 'development',
	db: 'mongodb://localhost/ecard', 
	port: 3000, 
	address: 'localhost', 
	domain: 'localhost:3000',
	urlConfig: {
		host: 'https://localhost:4200',
		confirmAccount:  'https://localhost:4200/confirmAccount/:token',
		forgotPassword:  'https://localhost:4200/forgotPassword/:token'
	}
};