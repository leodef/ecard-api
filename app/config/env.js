module.exports = function() {
    const  environment = (process.env.NODE_ENV || 'development');
	return require(`./env/${environment}.js`);
}