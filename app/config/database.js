const mongoose = require('mongoose');

const env = require('./env')();
//var connection;


module.exports = function(){
	mongoose.connect(env.db, { useMongoClient: true });
	
	if(env.env = 'development'){
		mongoose.set('debug', true);
	}
	//if(!connection){
	//	connection = mongoose.createConnection(uri);
	//}
	
	mongoose.connection.on('connected', function(){
		console.log('mongoose connected');
	});
	
	mongoose.connection.on('disconnected', function(){
		console.log('mongoose disconnected');
	});
	
	mongoose.connection.on('error', function(err){
		console.log('mongoose error: '+err);
	});
	
	//close db section when app closed
	process.on('SIGINT', function(){
		mongoose.connection.close(function(){
			console.log('Mongoose", desconnected by application')
		});
	})
	
	//return connection;
}