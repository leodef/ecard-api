const express =  require('express');
const apiRouter = require('./api');
const router = express.Router();

router.use('/api', apiRouter);

router.use('/',
    function(req, res){
        res.send('default');
});

module.exports = router;