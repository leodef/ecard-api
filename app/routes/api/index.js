const express = require('express');
//const adminRouter = require('./admin');
const personRouter = require('./person');
const router = express.Router();

//router.use('/admin', adminRouter);
router.use('/person', personRouter);

router.use('/', function(req, res){
    res.send('api');
});

module.exports = router;