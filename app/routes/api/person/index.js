const express = require('express');
const accountRouter = require('./account');
const cardRouter = require('./card');
const contactRouter = require('./contact');
const folderUserRouter = require('./folder-user');
const folderRouter = require('./folder');
const sectionFieldRouter = require('./section-field');
const sectionLocationRouter = require('./section-location');
const sectionRouter = require('./section');

const router = express.Router();

router.use('/account', accountRouter);
router.use('/card', cardRouter);
router.use('/contact', contactRouter);
router.use('/folder-user', folderUserRouter);
router.use('/folder', folderRouter);
router.use('/section-field', sectionFieldRouter);
router.use('/section-location', sectionLocationRouter);
router.use('/section', sectionRouter);

router.use('/', function(req, res){
    res.send('person');
});

module.exports = router;