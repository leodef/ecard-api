const passport = require('passport');
const express = require('express');
const personFolderService = require('../../../services/person/folder');
const router = express.Router();

//byFolder
router.get('/byFolder/:id',
    passport.authenticate('bearer', { session: false }),
    personFolderService.byFolder);

//root
router.post('/root',
    passport.authenticate('bearer', { session: false }),
    personFolderService.root);

//child
router.post('/child/:id',
    passport.authenticate('bearer', { session: false }),
    personFolderService.child);

//all
router.get('/',
    passport.authenticate('bearer', { session: false }),
    personFolderService.all);

//find
router.get('/:id',
    passport.authenticate('bearer', { session: false }),
    personFolderService.find);

//delete
router.delete('/:id',
    passport.authenticate('bearer', { session: false }),
    personFolderService.delete);

//update
router.put('/:id',
    passport.authenticate('bearer', { session: false }),
    personFolderService.update);


module.exports = router;