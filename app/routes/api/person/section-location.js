const passport = require('passport');
const express = require('express');
const personSectionLocationService = require('../../../services/person/section-location');
const router = express.Router();

//bySection
router.get('/bySection/:id',
    passport.authenticate('bearer', { session: false }),
    personSectionLocationService.bySection);

//bySection
router.get('/byCard/:id',
    passport.authenticate('bearer', { session: false }),
    personSectionLocationService.byCard);

module.exports = router;