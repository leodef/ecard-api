const express = require('express');
const personAccountService = require('../../../services/person/account');
const router = express.Router();

router.post('/login', personAccountService.login);
router.post('/logout', personAccountService.logout);
router.post('/register', personAccountService.register);
router.post('/confirm-account/:token', personAccountService.confirmAccount);
router.post('/forgot-password', personAccountService.forgotPassword);
router.post('/reset-password/:token', personAccountService.resetPassword);
router.get('/check-username/:username', personAccountService.checkUsername);

router.get('/', function(req, res){
    res.send('account');
});

module.exports = router;