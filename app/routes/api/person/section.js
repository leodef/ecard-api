const passport = require('passport');
const express = require('express');
const personSectionService = require('../../../services/person/section');
const router = express.Router();

//all
router.get('/',
    passport.authenticate('bearer', { session: false }),
    personSectionService.all);

//find
router.get('/:id',
    passport.authenticate('bearer', { session: false }),
    personSectionService.find);

//delete
router.delete('/:id',
    passport.authenticate('bearer', { session: false }),
    personSectionService.delete);

//create
router.post('/',
    passport.authenticate('bearer', { session: false }),
    personSectionService.create);

//update
router.put('/:id',
    passport.authenticate('bearer', { session: false }),
    personSectionService.update);


module.exports = router;