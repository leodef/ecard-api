const passport = require('passport');
const express = require('express');
const personContactService = require('../../../services/person/contact');
const router = express.Router();

//byFolder
router.get('/byFolder/:id',
    passport.authenticate('bearer', { session: false }),
    personContactService.byFolder);

//all
router.get('/',
    passport.authenticate('bearer', { session: false }),
    personContactService.all);

//find
router.get('/:id',
    passport.authenticate('bearer', { session: false }),
    personContactService.find);

//delete
router.delete('/:id',
    passport.authenticate('bearer', { session: false }),
    personContactService.delete);

//create
router.post('/',
    passport.authenticate('bearer', { session: false }),
    personContactService.create);

//update
router.put('/:id',
    passport.authenticate('bearer', { session: false }),
    personContactService.update);

//updateFolder
router.put('/folder/:id',
    passport.authenticate('bearer', { session: false }),
    personContactService.updateFolder);
    

module.exports = router;