const passport = require('passport');
const express = require('express');
const personSectionFieldService = require('../../../services/person/section-field');
const router = express.Router();

//bySection
router.get('/bySection/:id',
    passport.authenticate('bearer', { session: false }),
    personSectionFieldService.bySection);

module.exports = router;