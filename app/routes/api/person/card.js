const passport = require('passport');
const express = require('express');
const personCardService = require('../../../services/person/card');
const router = express.Router();

//rowByUser
router.get('/rowByUser',
    passport.authenticate('bearer', { session: false }),
    personCardService.rowByUser);

//all
router.get('/',
    passport.authenticate('bearer', { session: false }),
    personCardService.all);

//find
router.get('/:id',
    passport.authenticate('bearer', { session: false }),
    personCardService.find);

//delete
router.delete('/:id',
    passport.authenticate('bearer', { session: false }),
    personCardService.delete);

//create
router.post('/',
    passport.authenticate('bearer', { session: false }),
    personCardService.create);

//update
router.put('/:id',
    passport.authenticate('bearer', { session: false }),
    personCardService.update);

module.exports = router;