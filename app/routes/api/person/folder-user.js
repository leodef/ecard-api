const passport = require('passport');
const express = require('express');
const personFolderUserService = require('../../../services/person/folder-user');
const router = express.Router();

//byUser
router.get('/byUser',
    passport.authenticate('bearer', { session: false }),
    personFolderUserService.byUser);

module.exports = router;