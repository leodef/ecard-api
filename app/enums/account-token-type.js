const USER_TOKEN_TYPE = [
	'RESET_PASSWORD',
	'CONFIRM_ACCOUNT'
];

module.exports = USER_TOKEN_TYPE;