const ACCOUNT_TYPE = [
	'USER',
	'ADMIN',
	'ENTERPRISE'
];

module.exports = ACCOUNT_TYPE;
