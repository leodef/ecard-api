const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const folderUserSchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  isRoot: Boolean,
  user: {type: Schema.Types.ObjectId, ref: 'User'},
  folder: { type: Schema.Types.ObjectId, ref: 'Folder'}
}, {timestamps: true});
