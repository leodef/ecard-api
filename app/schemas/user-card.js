const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userCardSchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  card: { type: Schema.Types.ObjectId, ref: 'Card' }
}, {timestamps: true});