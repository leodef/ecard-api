const mongoose = require('mongoose');
const ACCOUNT_TYPE = require('../enums/account-type');
const gender = require('../enums/gender');
const Schema = mongoose.Schema;

const accountSchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  name: String,
  username: String,
  email: String,
  password: String,
  accountType: {type: String, enum: ACCOUNT_TYPE},
  gender: {type: String, enum: gender},
  birth: { type: Date, default: Date.now },
  profileImageURL: String,
  enabled: Boolean,
  grantedAuthority: [{ type: Schema.Types.ObjectId, ref: 'AccountGrantedAuthority' }],
  userTokenList: [{ type: Schema.Types.ObjectId, ref: 'UserToken' }]
}, {timestamps: true});