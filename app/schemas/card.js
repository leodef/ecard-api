const mongoose = require('mongoose');
const CARD_TYPE = require('../enums/card-type');
const Schema = mongoose.Schema;

const cardSchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  title: String,
  language: String,
  color: String,
  traking: Boolean,
  type: {type: String, enum: CARD_TYPE}, //  (BUSINESS_CARD, CUSTOM_LABEL, CATALOG, MENU)
  code: String,
  sectionLocationList: [{ type: Schema.Types.ObjectId, ref: 'SectionLocation' }]
}, {timestamps: true});