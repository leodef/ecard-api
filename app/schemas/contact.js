const mongoose = require('mongoose');
const CONTACT_TYPE = require('../enums/contact-type');
const Schema = mongoose.Schema;

const contactSchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  title: String,
  contactId: { type: Schema.Types.ObjectId, refPath: 'contactType' },  // id referente ao objeto do contato (cartão, pessoa ou empresa)
  contactType: {type: String, enum: CONTACT_TYPE}, //se é cartão pessoas ou empresa
  user: { type: Schema.Types.ObjectId, ref: 'User' }, // contact owner
  folder: { type: Schema.Types.ObjectId, ref: 'Folder' },
}, {timestamps: true});