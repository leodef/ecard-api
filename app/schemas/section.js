const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const sectionSchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  title: String,
  color: String,
  //orientation: String,
  columnN: Number,
  rowN: Number,
  sectionFieldList: [{ type: Schema.Types.ObjectId, ref: 'SectionField' }],
  sectionLocationList: [{ type: Schema.Types.ObjectId, ref: 'SectionLocation' }]
}, {timestamps: true});