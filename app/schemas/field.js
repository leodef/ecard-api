const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const fieldSchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  name: String,
  type: String,
  fieldValidatorList: [{ type: Schema.Types.ObjectId, ref: 'FieldValidator' }]
}, {timestamps: true});