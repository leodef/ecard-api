const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const accountGrantedAuthoritySchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  authority: String
}, {timestamps: true});
