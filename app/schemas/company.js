const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const companySchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  name: String,
  code: String,
  type: String,
  codeType: String,
  companyUserList: [{ type: Schema.Types.ObjectId, ref: 'CompanyUser' }],
  companyCardList: [{ type: Schema.Types.ObjectId, ref: 'CompanyCard' }]
}, {timestamps: true});