const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const sectionFieldSchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  column: Number,
  row: Number,
  value: String,
  title: String,
  field: { type: Schema.Types.ObjectId, ref: 'Field' }
}, {timestamps: true});
