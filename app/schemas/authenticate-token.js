const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const authenticateTokenSchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  expiryDate: { type: Date, default: Date.now },
  account: { type: Schema.Types.ObjectId, ref: 'Account' }
}, {timestamps: true});