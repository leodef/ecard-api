const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const fieldValidatorSchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  type: String,
  value: String,
  field: String
}, {timestamps: true});