const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  nickname: String,
  account: { type: Schema.Types.ObjectId, ref: 'Account' },
  userCardList: [{ type: Schema.Types.ObjectId, ref: 'UserCard' }],
  companyUserList: [{ type: Schema.Types.ObjectId, ref: 'CompanyUser' }]
}, {timestamps: true});