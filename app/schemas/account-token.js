const mongoose = require('mongoose');
const ACCOUNT_TOKEN_TYPE = require('../enums/account-token-type');
const Schema = mongoose.Schema;

const accountTokenSchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  expiryDate: Date,
  type: {type: String, enum: ACCOUNT_TOKEN_TYPE},
  account: { type: Schema.Types.ObjectId, ref: 'Account' }
}, {timestamps: true});
