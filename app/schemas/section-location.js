const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const sectionLocationSchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  position: String,
  section: { type: Schema.Types.ObjectId, ref: 'Section' }
}, {timestamps: true});