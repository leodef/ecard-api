const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const companyUserSchema = module.exports = Schema({
  //_id: Schema.Types.ObjectId, //If you have not declared it in schema, MongoDB will declare and initialize it
  company: { type: Schema.Types.ObjectId, ref: 'Company' },
  user: { type: Schema.Types.ObjectId, ref: 'User' }
}, {timestamps: true});